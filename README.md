### README ###
"Elaborazione delle Immagini per la Chirurgia Assistita", repository class 2018-2019

Notebook_0: Introduction to Python 

Notebook_1: Introduction to image procesing in Python

Notebook_2: Fourier

Notebook_3: Convolution

Notebook_4: Dicom

Notebook_5: Segmentation

Notebook_6: Morphological filters

Notebook_7: MABS

Notebook_8: Segmentation metrics

Notebook_9: Features

Notebook_10: Image segmentation

Notebook_11: Triangulation

Notebook_12: Marching cubes

Notebook_13: Landmark registration

Notebook_14: Camera Calibration
